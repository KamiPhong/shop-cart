import React, { Component } from 'react';
import { Grid, Typography } from '@material-ui/core';
import data from '../../Data'
import Item from '../../components/item/Item';
import './Home.css';

export class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ItemList: []
        }
    }

    componentDidMount() {
        this.setState({
            ItemList: data
        })
    }

    render() {
        return (
            <div>
                <Typography variant="h3" gutterBottom className='panner-home-heading'>
                    Our items
                </Typography>
                <br />
                <Grid container spacing={6}>
                    {this.state.ItemList.map(item => (
                        <Grid item lg={3} md={3}>
                            <Item item={item}></Item>
                        </Grid>
                    ))}
                </Grid>
            </div>

        )
    }
}

export default Home
