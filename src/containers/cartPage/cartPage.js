import React, { Component } from 'react';
import { Grid, Typography } from '@material-ui/core';
import Product from '../../components/cart/Product';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import './cartPage.css';
import { UseContext } from '../../context/useContext'

export class CartPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            ItemList: [],
            totalPay: 0,
            extraShippingPrice: 0
        }

        this.shippingPrice = 6;
    }


    componentDidMount() {
        this.setState({
            ItemList: this.props.theContext.values,
        })
    }

    handleChange = (event) => {
        this.setState({
            extraShippingPrice: event.target.checked ? this.shippingPrice : 0
        });
    }

    handelTotalPay = () => {
        let { ItemList, extraShippingPrice } = this.state;
        let totalPay = 0;
        for (let i of ItemList) {
            totalPay += i.price * i.count;
        }

        return totalPay + extraShippingPrice;
    };

    handleAlertPay = () => {
        if (this.handelTotalPay() === 0) {
            alert("Bạn chưa mua sản phẩm nào! ");
            return;
        }
        alert("Bạn cần thanh toán : " + this.handelTotalPay());
        this.setState({
            ItemList: [],
            totalPay: 0
        })
    }

    render() {
        const { shippingPrice } = this;
        return (
            <div className='cart-page-root'>
                <Typography variant="h4" gutterBottom>
                    You have ordered:
                </Typography>
                <Grid container spacing={1}>
                    {this.state.ItemList.map(item => (
                        <Grid item lg={12}>
                            <Product item={item}
                            ></Product>

                        </Grid>
                    ))}
                </Grid>
                <Paper variant="outlined" square className='cart-page-pay-end'>
                    <Checkbox
                        onChange={this.handleChange}
                    /> Shipping (+{shippingPrice}$)
                    <hr />
                    <p><b>&emsp;Total: {this.handelTotalPay()}$</b></p>
                </Paper>

                <Button variant="contained" color="primary" className='cart-page-button-remove' onClick={() => this.handleAlertPay()}>
                    CHECKOUT
                </Button>
                <br />
            </div>

        )
    }
}

export default UseContext(CartPage);
