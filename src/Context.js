import React from 'react';
const default_state ={
    values:'',
    setValue:null,
    setTotalAfterRemove: null,
    increaseProduct: null,
    reductionProduct: null
} 
const ThemeContext = React.createContext(default_state);
export default ThemeContext;