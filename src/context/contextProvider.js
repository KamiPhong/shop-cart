import React from 'react';
import Context from './context';

export default class ContextProvider extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            values: [],
            totalValue: 0,
            setValue: this.setValue.bind(this)
        }
    }
    //set giá trị của mảng Item
    setValue(values) {
        const totalValue = this.handleSumTotalProduct(values);
        this.setState({
            values,
            totalValue
        })
    }
    
    //tính tổng số lượng hàng trong giỏ hàng
    handleSumTotalProduct = (values) => {
        let totalProduct = 0;
        for(let i of values) {
            totalProduct += i.count;
        }

        return totalProduct;
    }

    render(){
        return(
            <Context.Provider value={{
                theContext: {
                    ...this.state
                }
            }}>
                {this.props.children}
            </Context.Provider>
        )
    }

}