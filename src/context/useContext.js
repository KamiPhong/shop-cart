import React from 'react';
import Context from './context';
export function UseContext(Component){
    return function ContextComponent(props){
        return (
            <Context.Consumer>
                {(contexts)=>{
                    return <Component {...props} {...contexts} />
                }}
            </Context.Consumer>
        )
    }
}
