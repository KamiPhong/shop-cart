import * as React from 'react'
const default_state={
    values:'',
    setValue: ()=>{}
}
const Context = React.createContext(default_state);
export default Context;
