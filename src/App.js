import React, { Component } from 'react'
import Navbar from '../src/components/navbar/Navbar'
import Home from '../src/containers/home/Home'
import { Container } from '@material-ui/core'
import CartPage from '../src/containers/cartPage/cartPage';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import ContextProvider from '../src/context/contextProvider'
export class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      total: 0,
      listItem: [],
    }
  }
  
  render() {
    return (
      <Router>
        <ContextProvider>
          <div>
            <Navbar />
            <br />
            <Container maxWidth="lg">
              <Switch>
                <Route exact path="/">
                  <Home />
                </Route>
                <Route path="/cart">
                  <CartPage />
                </Route>
              </Switch>
            </Container>
          </div>
        </ContextProvider>
      </Router>
    )
  }
}

export default App

