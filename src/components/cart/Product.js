import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import Button from '@material-ui/core/Button';
import './Product.css';
import { UseContext } from '../../context/useContext'

export class Product extends Component {
	handleRemoveProductClick = (item) => {
		let listItem = this.props.theContext.values;
		if (listItem.length > 0) {
			for (let i = 0; i < listItem.length; i++) {
				if (listItem[i].id === item.id) {
					listItem.splice(i, 1);
					break;
				}
			};
		}
		this.props.theContext.setValue(listItem);

	}

	handleIncreaseProduct = (item) => {
		let listItem = this.props.theContext.values;
		const indexProd = listItem.indexOf(item);

		if (listItem[indexProd].count >= item.quantity) {
			alert(item.name + " chỉ còn " + item.quantity);
			return
		}
		else {
			listItem[indexProd].count += 1;
		}
		this.props.theContext.setValue(listItem);

	}


	handleReductionProduct = (item) => {
		let listItem = this.props.theContext.values;
		const indexProd = listItem.indexOf(item);

		listItem[indexProd].count -= 1;
		if (listItem[indexProd].count === 0) {
			this.handleRemoveProductClick(listItem[indexProd]);
			return;
		}
		this.props.theContext.setValue(listItem);

	}
	render() {
		const { item } = this.props;
		return (
			<Card className='product-root'>
				<CardMedia
					className='product-cover'
					image={item.avatar}
					title="Live from space album cover"
					height="160"

				/>
				<div className='product-details'>
					<CardContent className='product-content'>
						<Typography component="h5" variant="h5" color="Secondary">
							{item.name}
						</Typography>
						<Typography variant="subtitle1" color="textSecondary">
							{item.desc}
						</Typography>
						<Typography variant="subtitle1" color="primary"><b>
							{item.price} $
						</b>
						</Typography>
						<Typography variant="subtitle1" color="primary"><b>
							Quantity: {item.count}
						</b>
						</Typography>
					</CardContent>
					<div className='product-control'>
						<ArrowDropUpIcon className='product-icon' onClick={() => this.handleIncreaseProduct(item)} />&emsp;
						<ArrowDropDownIcon className='product-icon' onClick={() => this.handleReductionProduct(item)} />
					</div>
					<Button variant="contained" color="secondary" className='product-button-remove' onClick={() => this.handleRemoveProductClick(item)}>
						Remove
          			</Button>
				</div>

			</Card>
		);
	}
}
export default UseContext(Product);
