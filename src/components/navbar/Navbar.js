import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import Chip from '@material-ui/core/Chip';
import './Navbar.css';
import { Link } from "react-router-dom";
import  {UseContext} from '../../context/useContext';

export class NavBar extends Component {

    render() {
        return (
            <div className="navbar">
                <AppBar position="static">
                    <Toolbar>
                        <IconButton edge="start" color="inherit" aria-label="menu">
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h5" className="home-title">
                            <Link to="/" className="home-title--color">
                                Shopping
                            </Link>
                        </Typography>
                        <Button color="inherit" className='header-item-home'>
                            <Link to="/" >Shop</Link>
                        </Button>
                        <Button color="inherit">
                            <Link to="/cart">My cart</Link>
                        </Button>
                        <p>&emsp;</p>
                        <Link to="/cart">
                            <ShoppingCartIcon className="header-item-shopping-icon" />
                        </Link>
                        <Chip label={this.props.theContext.totalValue} color="secondary" className='header-item-amount-product' size="small" />

                    </Toolbar>
                </AppBar>
            </div>
        );
    }
}

export default UseContext(NavBar)
