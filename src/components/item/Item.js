import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Typography from '@material-ui/core/Typography';
import './Item.css';
import {UseContext} from '../../context/useContext'

export class Item extends Component {
    handleClickSubmit = (item) => {
        let listItem = this.props.theContext.values;

        const indexItem = listItem.indexOf(item);
        if(indexItem === -1){
            item.count = 1;
            listItem.push(item);
        }
        else{
            if (listItem[indexItem].count>=item.quantity) {
                alert(item.name + " chỉ còn " + item.quantity + " mà bạn chọn trong giỏ hàng");
            } else {
                listItem[indexItem].count++;
            }
        }

        this.props.theContext.setValue(listItem);
    }

    render() {
        let { item } = this.props;
        return (

            <Card className='card'>
                <CardActionArea onClick={() => this.handleClickSubmit(item)}>
                    <CardMedia
                        className="card-media"
                        component="img"
                        alt="Contemplative Reptile"
                        height="200"
                        image={item.avatar}
                        title="Contemplative Reptile"
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {item.name}
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {item.desc}
                        </Typography>
                        <p className='card-item card-item__price'><b>Price: {item.price} $</b></p>
                        <p className='card-item card-item__amount'>Amount: {item.quantity}</p>
                    </CardContent>
                </CardActionArea>
                <Fab color="secondary" className='card-add-item' onClick={() => this.handleClickSubmit(item)}>
                    <AddIcon />
                </Fab>
            </Card>
        );
    }
}

export default UseContext(Item);
