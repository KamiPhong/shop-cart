const Items = [
    {
        id: 1,
        avatar:"https://i.pinimg.com/236x/2e/06/83/2e06833f1d8a0f3fd8ff602e60be77d5.jpg",
        name: "Lizardon",
        desc:"Lizards are a widespread group of squamate reptiles, with over 6,000 species, rangingacross all continents except Antarctica",
        price: 500, 
        quantity: 4
    },
    {
        id: 2,
        avatar:"https://www.converse.com/on/demandware.static/-/Sites-ConverseMaster/default/dw24e2936a/images/hi-res/M9160C_standard.jpg",
        name: "Pikachu",
        desc:"Lizards are a widespread group of squamate reptiles, with over 6,000 species, rangingacross all continents except Antarctica",
        price: 700,
        quantity: 2

    },
    {
        id: 3,
        avatar:"https://www.converse.com/on/demandware.static/-/Sites-ConverseMaster/default/dw24e2936a/images/hi-res/M9160C_standard.jpg",
        name: "Geninja",
        desc:"Lizards are a widespread group of squamate reptiles, with over 6,000 species, rangingacross all continents except Antarctica",
        price: 250,
        quantity: 50

    },
    {
        id: 4,
        avatar:"https://www.converse.com/on/demandware.static/-/Sites-ConverseMaster/default/dw24e2936a/images/hi-res/M9160C_standard.jpg",
        name: "Psyduck",
        desc:"Lizards are a widespread continents except Antarctica",
        price: 200523,
        quantity: 50

    },
    {
        id: 5,
        avatar:"https://www.converse.com/on/demandware.static/-/Sites-ConverseMaster/default/dw24e2936a/images/hi-res/M9160C_standard.jpg",
        name: "Meoth",
        desc:"Lizards are a widespread group of squamate reptiles, with over 6,000 species, rangingacross all continents except Antarctica",
        price: 200234,
        quantity: 50

    },
    {
        id: 6,
        avatar:"https://www.converse.com/on/demandware.static/-/Sites-ConverseMaster/default/dw24e2936a/images/hi-res/M9160C_standard.jpg",
        name: "Togepi",
        desc:"Lizards are a widespread group of squamate reptiles, with over 6,000 species, rangingacross all continents except Antarctica",
        price: 2040,
        quantity: 50

    },
];

export default Items;